'use strict';
exports.getResult = (function_id,game_data,selection) => {
    return betconstructFunctions[function_id](game_data,selection)??null;
};
const betconstructFunctions = {
    //Funciones globales
    "altaBajaDosOpciones":function(base,total,type,finalizado){
        let respuesta = 0;
        if(base == total){
        //si son iguales
            if(finalizado){ 
                respuesta = 3;
            }
            else{
                respuesta = 0;
            }
        }
        else{
            //si son diferentes
            if(type == "Over"){
                if(base < total){
                    respuesta = 1;
                }
                if(base > total){
                    if(finalizado){
                        respuesta = 2;
                    }
                    else{
                        respuesta = 0;
                    }
                }
            }
            if(type == "Under"){
                if(base > total){
                    if(finalizado){
                        respuesta = 1;
                    }else{
                        respuesta = 0;
                    }
                }
                if(base < total){
                    respuesta = 2;
                }
            }
        }
        return respuesta;
    },
    "AltaBajaTresOpciones":function(base,total,type,finalizado){
        if(type=="Over"){
            if(finalizado){
               return (total>base) ? 1 : 2;
            }
            return (total>base) ? 1 : 0;
        }
        else if(type=="Under"){
            if(finalizado){
                return (total<base) ? 1 : 2;
            }
            return (total>base) ? 2 : 0;
        }
        else if(type=="Exactly"){
            if(finalizado){
                return (total==base) ? 1 : 2;
            }
            return (total>base) ? 2 : 0;
        }
    },
    "HandicapBase":function(selection,base,p1,p2){
        if(selection == "Home"){
            p1 = p1 + base;
            if(Math.floor(base)!=base){
                return (p1>p2) ? 1 : 2;
            }
            else{
                if(p1>p2){return 1;}
                if(p1<p2){return 2;}
                if(p1==p2){return 3;}
            }
            return 0;
        }
        else{
            p2 = p2 + base;
            if(Math.floor(base)!=base){
                return (p1<p2) ? 1 : 2;
            }
            else{
                if(p1<p2){return 1;}
                if(p1>p2){return 2;}
                if(p1==p2){return 3;}
            }
            return 0;
        }
    },
    "Handicap3opcionesBase":function(selection,base,p1,p2){
        if(selection == "Home"){
            p1 = p1 + base;
            return (p1>p2) ? 1 : 2;
        }
        else if(selection == "Away"){
            p2 = p2 + base;
            return (p1<p2) ? 1 : 2;
        }
        else if(selection == "Tie"){
            p2 = p2 + base;
            return (p1==$p2) ? 1 : 2;
        }
        return 0;
    },
    //-------------------Soccer-----------------
    //Resultado del partido P1XP2
    "346":function(data,select){
        if(data['current_game_state'] == 'finished'){
            let marcadorP1 = parseInt(data['stats']['score_set1']['team1_value']) + parseInt(data['stats']['score_set2']['team1_value']);
            let marcadorP2 = parseInt(data['stats']['score_set1']['team2_value']) + parseInt(data['stats']['score_set2']['team2_value']);
            let selection = select['type1']??select['type_1']??select['type'];
            if(selection=='P1' || selection == 'W1'){
               return (marcadorP1 > marcadorP2) ? 1 : 2;
            }
            if(selection=='P2' || selection == 'W2'){
                return (marcadorP1 < marcadorP2) ? 1 : 2;
            }
            if(selection=='X' || selection == "Empate" || selection=="Draw"){
                return (marcadorP1 == marcadorP2) ? 1 : 2;
            }
            return 0;
        }
        return 0;
    },
    //Doble chance - 192
    "192": function(data,select){
        if(data['current_game_state'] == 'finished'){
            let marcadorP1 = parseInt(data['stats']['score_set1']['team1_value']) + parseInt(data['stats']['score_set2']['team1_value']);
            let marcadorP2 = parseInt(data['stats']['score_set1']['team2_value']) + parseInt(data['stats']['score_set2']['team2_value']);
            let selection =  select['type1']??select['type_1']??select['type'];
            if(selection=='1X'){
               return (marcadorP1 > marcadorP2 || marcadorP1 == marcadorP2) ? 1 : 2;
            }
            if(selection=='12'){
                return (marcadorP1 > marcadorP2 || marcadorP1 < marcadorP2) ? 1 : 2;
            }
            if(selection=='X2'){
                return (marcadorP1 == marcadorP2 || marcadorP1 < marcadorP2) ? 1 : 2;
            }
            return 0;
        }
        return 0;
    },
    //Ambos equipos marcan - 214
    "214": function(data,select){
        marcadorP1 = parseInt(data['stats']['score_set1']['team1_value']) + (('score_set2' in data['stats']) ? parseInt(data['stats']['score_set2']['team1_value']):0);
        marcadorP2 = parseInt(data['stats']['score_set1']['team2_value']) + (('score_set2' in data['stats']) ? parseInt(data['stats']['score_set2']['team2_value']):0);
        if(select['type']=='Yes'){
            if(data['current_game_state'] == 'finished'){
                return (marcadorP1 > 0 && marcadorP2 > 0) ? 1 : 2;
            }
            return (marcadorP1 > 0 && marcadorP2 > 0) ? 1 : 0;
        }
        else{
            if(data['current_game_state'] == 'finished'){
                return (marcadorP1 > 0 && marcadorP2 > 0) ? 2 : 1;
            }
            return (marcadorP1 > 0 && marcadorP2 > 0) ? 2 : 0;
        }
        return 0;
    },
    //Total de goles Par/Impar - 234
    "234": function(data,select){
        if(data['current_game_state'] == 'finished'){
            marcadorP1 = parseInt(data['stats']['score_set1']['team1_value']) + parseInt(data['stats']['score_set2']['team1_value']);
            marcadorP2 = parseInt(data['stats']['score_set1']['team2_value']) + parseInt(data['stats']['score_set2']['team2_value']);
            if(select['type']=="Even"){
                return ((marcadorP1+marcadorP2) % 2 === 0) ? 1 : 2;
            }
            if(select['type']=="Odd"){
                return ((marcadorP1+marcadorP2) % 2 != 0) ? 1 : 2;
            }
        }
        return 0;
    },
    //Marcador Correcto - 227
    "227": function(data,select){
        if(data['current_game_state'] == 'finished'){
            marcadorP1 = parseInt(data['stats']['score_set1']['team1_value']) + parseInt(data['stats']['score_set2']['team1_value']);
            marcadorP2 = parseInt(data['stats']['score_set1']['team2_value']) + parseInt(data['stats']['score_set2']['team2_value']);
            selectionP1 = parseInt(('home_value' in select) ? select['home_value'] : (('home_value' in select) ? select['home_value'] : select['name'].split('-')[0]));
            selectionP2 = parseInt(('away_value' in select) ? select['away_value'] : (('away_value' in select) ? select['away_value'] : select['name'].split('-')[1]));
            return (marcadorP1 == selectionP1 && marcadorP2 == selectionP2) ? 1 : 2;
        }
        return 0;
    },
    "659": function(data,select){
        if(data['current_game_state'] == 'finished'){
            marcadorP1 = parseInt(data['stats']['score_set1']['team1_value']) + parseInt(data['stats']['score_set2']['team1_value']);
            marcadorP2 = parseInt(data['stats']['score_set1']['team2_value']) + parseInt(data['stats']['score_set2']['team2_value']);
            type = (('type1' in select)) ? select['type1'] : ((('type_1' in select)) ? select['type_1'] : select['type']);
            selectionP1 = type.split('/')[0];
            selectionP11 = selectionP1.split('-')[0];
            selectionP12 = selectionP1.split('-')[1];
            selectionP2 = type.split('/')[1];
            selectionP21 = selectionP2.split('-')[0];
            selectionP22 = selectionP2.split('-')[1];
            selectionP3 = type.split('/')[2];
            selectionP31 = selectionP3.split('-')[0];
            selectionP32 = selectionP3.split('-')[1];
            return ((marcadorP1 == selectionP11 && marcadorP2 == selectionP12) || (marcadorP1 == selectionP21 && marcadorP2 == selectionP22) || (marcadorP1 == selectionP31 && marcadorP2 == selectionP32)) ? 1 : 2;
        }
        return 0;
    },
    "231": function(data,select){
        if(data['current_game_state'] == 'finished'){
            marcadorP1 = parseInt(data['stats']['score_set1']['team1_value']) + parseInt(data['stats']['score_set2']['team1_value']);
            marcadorP2 = parseInt(data['stats']['score_set1']['team2_value']) + parseInt(data['stats']['score_set2']['team2_value']);
            if(select['type']=="Home"){
                if(marcadorP1 > marcadorP2){return 1;}
                else if(marcadorP1 < marcadorP2){return 2;}
                else if(marcadorP1 == marcadorP2){return 3;}
            }
            else{
                if(marcadorP1 < marcadorP2){return 1;}
                else if(marcadorP1 > marcadorP2){return 2;}
                else if(marcadorP1 == marcadorP2){return 3;}
            }
        }
        return 0;
    },
    //Cada equipo Anotará mas de X goles - 232
    "232": function(data,select){
        base = parseFloat(select['base']);
        selection = (('type1' in select)) ? select['type1'] : select['type_1'];
        marcadorP1_1=parseInt(data['stats']['score_set1']['team1_value']);
        marcadorP1_2=(('score_set2' in data['stats'])) ? parseInt(data['stats']['score_set2']['team1_value']) : 0;
        marcadorP2_1=parseInt(data['stats']['score_set1']['team2_value']);
        marcadorP2_2=(('score_set2' in data['stats'])) ? parseInt(data['stats']['score_set2']['team2_value']) : 0;
        total_P1 = marcadorP1_1 + marcadorP1_2;
        total_P2 = marcadorP2_1 + marcadorP2_2;
        if(selection == "Yes"){
            if(data['current_game_state'] == 'finished'){
                return (total_P1>base && total_P2>base) ? 1 : 2;
            }
            return (total_P1>base && total_P2>base) ? 1 : 0;
        }
        else{
            if(data['current_game_state'] == 'finished'){
                return (total_P1<base && total_P2<base) ? 1 : 2;
            }
            return (total_P1>base || total_P2>base) ? 2 : 0;
        }
        return 0;
    },
    //Cada equipo Anotará menos de X goles - 233
    "233": function(data,select){
        base = parseFloat(select['base']);
        selection = (('type1' in select)) ? select['type1'] : select['type_1'];
        marcadorP1_1=parseInt(data['stats']['score_set1']['team1_value']);
        marcadorP1_2=(('score_set2' in data['stats'])) ? parseInt(data['stats']['score_set2']['team1_value']) : 0;
        marcadorP2_1=parseInt(data['stats']['score_set1']['team2_value']);
        marcadorP2_2=(('score_set2' in data['stats'])) ? parseInt(data['stats']['score_set2']['team2_value']) : 0;
        total_P1 = marcadorP1_1 + marcadorP1_2;
        total_P2 = marcadorP2_1 + marcadorP2_2;
        if(selection == "No"){
            if(data['current_game_state'] == 'finished'){
                return (total_P1>base && total_P2>base) ? 1 : 2;
            }
            return (total_P1>base && total_P2>base) ? 1 : 0;
        }
        else{
            if(data['current_game_state'] == 'finished'){
                return (total_P1<base && total_P2<base) ? 1 : 2;
            }
            return (total_P1>base || total_P2>base) ? 2 : 0;
        }
        return 0;
    },
    "403": function(data,select){
        if(data['current_game_state'] == 'finished'){
            marcadorP1 = parseInt(data['stats']['score_set1']['team1_value']) + parseInt(data['stats']['score_set2']['team1_value']);
            marcadorP2 = parseInt(data['stats']['score_set1']['team2_value']) + parseInt(data['stats']['score_set2']['team2_value']);
            selection = (('type1' in select)) ? select['type1'] : ((('type_1' in select)) ? select['type_1'] : select['type']);
            if(selection=='Yes'){
                return (marcadorP1 > marcadorP2 && marcadorP2 == 0) ? 1 : 2;
            }
            else{
                return (marcadorP1 > marcadorP2 && marcadorP2 > 0) ? 1 : 2;
            }
            return 0;
        }
        return 0;
    },
    "421": function(data,select){
        if(data['current_game_state'] == 'finished'){
            marcadorP1 = parseInt(data['stats']['score_set1']['team1_value']) + parseInt(data['stats']['score_set2']['team1_value']);
            marcadorP2 = parseInt(data['stats']['score_set1']['team2_value']) + parseInt(data['stats']['score_set2']['team2_value']);
            selection = (('type1' in select)) ? select['type1'] : ((('type_1' in select)) ? select['type_1'] : select['type']);
            if(selection=='Yes'){
                return (marcadorP1 < marcadorP2 && marcadorP1 == 0) ? 1 : 2;
            }
            else{
                return (marcadorP1 < marcadorP2 && marcadorP1 > 0) ? 1 : 2;
            }
            return 0;
        }
        return 0;
    },
    //Equipo 1 gana por 2 o 3 goles - 407
    "407": function(data,select){
        if(data['current_game_state'] == 'finished'){
            marcadorP1 = parseInt(data['stats']['score_set1']['team1_value']) + parseInt(data['stats']['score_set2']['team1_value']);
            marcadorP2 = parseInt(data['stats']['score_set1']['team2_value']) + parseInt(data['stats']['score_set2']['team2_value']);
            selection = (('type1' in select)) ? select['type1'] : ((('type_1' in select)) ? select['type_1'] : select['type']);
            if(selection=='Yes'){
                return (marcadorP1 - marcadorP2 == 2 || marcadorP1 - marcadorP2 == 3) ? 1 : 2;
            }
            else{
                return (marcadorP1 - marcadorP2 != 2 || marcadorP1 - marcadorP2 != 3) ? 1 : 2;
            }
            return 0;
        }
        return 0;
    },
    //Equipo 2 gana por 2 o 3 goles de diferencia - 425
    "425": function(data,select){
        if(data['current_game_state'] == 'finished'){
            marcadorP1 = parseInt(data['stats']['score_set1']['team1_value']) + parseInt(data['stats']['score_set2']['team1_value']);
            marcadorP2 = parseInt(data['stats']['score_set1']['team2_value']) + parseInt(data['stats']['score_set2']['team2_value']);
            selection = (('type1' in select)) ? select['type1'] : ((('type_1' in select)) ? select['type_1'] : select['type']);
            if(selection=='Yes'){
                return (marcadorP2 - marcadorP1 == 2 || marcadorP2 - marcadorP1 == 3) ? 1 : 2;
            }
            else{
                return (marcadorP2 - marcadorP1 != 2 || marcadorP2 - marcadorP1 != 3) ? 1 : 2;
            }
            return 0;
        }
        return 0;
    },
    //Equipo 1 gana por un gol de diferencia o empata - 406
    "406": function(data,select){
        if(data['current_game_state'] == 'finished'){
            marcadorP1 = parseInt(data['stats']['score_set1']['team1_value']) + parseInt(data['stats']['score_set2']['team1_value']);
            marcadorP2 = parseInt(data['stats']['score_set1']['team2_value']) + parseInt(data['stats']['score_set2']['team2_value']);
            selection = (('type1' in select)) ? select['type1'] : ((('type_1' in select)) ? select['type_1'] : select['type']);
            if(selection=='Yes'){
                return (marcadorP1 - marcadorP2 == 1 || marcadorP1 == marcadorP2) ? 1 : 2;
            }
            else{
                return (marcadorP1 - marcadorP2 != 1 || marcadorP1 != marcadorP2) ? 1 : 2;
            }
            return 0;
        }
        return 0;
    },
    //Equipo 2 gana por un gol de diferencia o empata - 424
    "424": function(data,select){
        if(data['current_game_state'] == 'finished'){
            marcadorP1 = parseInt(data['stats']['score_set1']['team1_value']) + parseInt(data['stats']['score_set2']['team1_value']);
            marcadorP2 = parseInt(data['stats']['score_set1']['team2_value']) + parseInt(data['stats']['score_set2']['team2_value']);
            selection = (('type1' in select)) ? select['type1'] : ((('type_1' in select)) ? select['type_1'] : select['type']);
            if(selection=='Yes'){
                return (marcadorP2 - marcadorP1 == 1 || marcadorP2 == marcadorP1) ? 1 : 2;
            }
            else{
                return (marcadorP2 - marcadorP1 != 1 || marcadorP2 != marcadorP1) ? 1 : 2;
            }
            return 0;
        }
        return 0;
    },
    //Equipo 2 gana por un gol de diferencia o empata - 424
    "424": function(data,select){
        if(data['current_game_state'] == 'finished'){
            marcadorP1 = parseInt(data['stats']['score_set1']['team1_value']) + parseInt(data['stats']['score_set2']['team1_value']);
            marcadorP2 = parseInt(data['stats']['score_set1']['team2_value']) + parseInt(data['stats']['score_set2']['team2_value']);
            selection = (('type1' in select)) ? select['type1'] : ((('type_1' in select)) ? select['type_1'] : select['type']);
            if(selection=='Yes'){
                return (marcadorP2 - marcadorP1 == 1 || marcadorP2 == marcadorP1) ? 1 : 2;
            }
            else{
                return (marcadorP2 - marcadorP1 != 1 || marcadorP2 != marcadorP1) ? 1 : 2;
            }
            return 0;
        }
        return 0;
    },
    //Resultado del partido = Empate de goles - 329
    "329": function(data,select){
        if(data['current_game_state'] == 'finished'){
            marcadorP1 = parseInt(data['stats']['score_set1']['team1_value']) + parseInt(data['stats']['score_set2']['team1_value']);
            marcadorP2 = parseInt(data['stats']['score_set1']['team2_value']) + parseInt(data['stats']['score_set2']['team2_value']);
            selection = (('type1' in select)) ? select['type1'] : ((('type_1' in select)) ? select['type_1'] : select['type']);
            if(selection=='Yes'){
                return (marcadorP1 == marcadorP2) ? 1 : 2;
            }
            else{
                return (marcadorP1 != marcadorP2) ? 1 : 2;
            }
            return 0;
        }
        return 0;
    },
}